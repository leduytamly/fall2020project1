package movies.importer;
/**
 * Class is responsible for processing all the files
 * @author Le Duytam
 */
import java.io.IOException;

public class ImportPipeline {
	public static void main(String[] args) {
		String kaggleSource = "C:\\Users\\leduy\\Google Drive\\T3 CS\\Java 3\\Projects\\Project_1_MovieImporter\\Test_files_kaggle";
		String imdbSource = "C:\\Users\\leduy\\Google Drive\\T3 CS\\Java 3\\Projects\\Project_1_MovieImporter\\Test_files_imdb";
		String importDestination = "C:\\Users\\leduy\\Google Drive\\T3 CS\\Java 3\\Projects\\Project_1_MovieImporter\\Test_files_processed";
		String normalizerDestination = "C:\\Users\\leduy\\Google Drive\\T3 CS\\Java 3\\Projects\\Project_1_MovieImporter\\Test_files_normalized";
		String validatorDestination = "C:\\Users\\leduy\\Google Drive\\T3 CS\\Java 3\\Projects\\Project_1_MovieImporter\\Test_files_validated";
		String deduperDestination = "C:\\Users\\leduy\\Google Drive\\T3 CS\\Java 3\\Projects\\Project_1_MovieImporter\\Test_files_deduper";
		/*
		String kaggleSource = "C:\\Users\\Lili\\courses\\java310\\kaggle_testfiles";
		String imdbSource = "C:\\Users\\Lili\\courses\\java310\\imdb_testfiles";
		String importDestination = "C:\\Users\\Lili\\courses\\java310\\testfiles_processed";
		String normalizerDestination = "C:\\Users\\Lili\\courses\\java310\\testfiles_normalized";		
		String validatorDestination = "C:\\Users\\Lili\\courses\\java310\\testfiles_validator";
		String deduperDestination = "C:\\Users\\Lili\\courses\\java310\\testfiles_deduper";
		*/
		
		// Creating objects for each class
		
		KaggleImporter ki = new KaggleImporter(kaggleSource,importDestination);
		ImdbImporter ii = new ImdbImporter(imdbSource,importDestination);
		Normalizer n = new Normalizer(importDestination,normalizerDestination);
		Validator v = new Validator(normalizerDestination,validatorDestination);
		Deduper d = new Deduper(validatorDestination,deduperDestination);
		
		Processor[] processors = {ki,ii,n,v,d};
		try {
			processAll(processors);
		} catch (IOException e) {
			System.out.println("An error occured");
		}
	}
	
	/**
	 *  Method processing all the files by calling the Processor class
	 *  @param Processor[] array that was created in the main method
	 */
	public static void processAll(Processor[] processors) throws IOException {
		for(Processor p : processors) {
			p.execute();
		}
	}
}
