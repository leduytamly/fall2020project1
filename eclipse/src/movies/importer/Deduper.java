package movies.importer;
/**
 * Class is responsible for merging duplicates together
 * @author Le Duytam, Liliane
 */

import java.util.ArrayList;
import java.util.HashSet;

public class Deduper extends Processor{
	private final int RELEASE_YEAR_COLUMNS = 2;
	private final int TITLE_COLUMNS = 0;
	private final int RUNTIME_COLUMN = 1;
	private final int SOURCE = 3;
	
	/**
	 * Constructor for Deduper
	 * @param sourceDir Source directory
	 * @param outputDir Output directory
	 */
	public Deduper(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	/**
	 * 	Overrides the process method. It merges duplicates into one entry and concatenates to source 
	 * @return ArrayList with all entries with no duplicates  
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> processed = new ArrayList<String>();
		ArrayList<Movie> mov = new ArrayList<Movie>();
		
		//Loop to split the every line of the file 
		for(String s : input) {
			String[] section = s.split("\\t", -1);
			//Creates a new movie
			Movie newMov = new Movie(section[RELEASE_YEAR_COLUMNS], section[TITLE_COLUMNS], section[RUNTIME_COLUMN], section[SOURCE]);
			
			//Checks if the movie is already in the ArrayList<Movie>
			if(mov.contains(newMov)) {
				//If it is in the ArrayList<Movie>Gets the index of the repeated movie
				int index = mov.indexOf(newMov);
				
				//Concatenates the source for the repeated movie
				mov.get(index).setSource(mov.get(index).getSource(),newMov.getSource());
				
				//Removes any duplicate sources
				String newSource = removeDuplicate(mov.get(index).getSource());
				
				//Sets the source without duplicates for the movie in the ArrayList<Movie>
				mov.get(index).setSource(newSource);
				
				//Sets the Movie's toString in the ArrayList<String> at the repeated index
				processed.set(index, mov.get(index).toString());
				
			} else {
				//If it is not in the ArrayList<Movie>, adds the movie in both ArrayLists
				mov.add(newMov);
				processed.add(newMov.toString());
			}
		}
		return processed;
	}
	
	/**
	 * Removes duplicates from a string and concatenates them with a ";"
	 * @param input
	 * @return String with no duplicates separated by a ";"
	 */
	public String removeDuplicate(String input) {
		HashSet<String> hs = new HashSet<String>();
		
		//Splits the input
		String[] inputSplit = input.split(";",-1);
		
		//Loops through the String array and adds it to the HashSet(Duplicates will be removed)
		for (String word : inputSplit) {
				hs.add(word);
		}
		
		String result = "";
		int i = 0;
		//Loops through the HashSet to concatenate the sources with a ";"
		for (String s : hs) {	
			if (i++ < hs.size()-1) {
				result+=s+";";
			} else {
				//Does not append the ";" if it is the last element in the HashSet
				result+=s;
			}
		}
		return result;
	}

}
