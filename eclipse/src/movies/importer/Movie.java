package movies.importer;
/**
 * Represents a movie which has a release year, name, runtime, and source
 *
 */
public class Movie {
	private String releaseYear;
	private String name;
	private String runtime;
	private String source;
	
	/**
	 * Constructor for movie
	 * @param releaseYear The release year of the movie
	 * @param name The title of the movie
	 * @param runtime The length in minutes of the movie 
	 * @param source The source of the movie (kaggle or imdb)
	 * @author Le Duytam 
	 */
	public Movie(String releaseYear, String name, String runtime, String source) {
		this.releaseYear = releaseYear;
		this.name = name;
		this.runtime = runtime;
		this.source = source;
	}
	
	/**
	 * Getter for source
	 * @return The source of the movie(kaggle or imdb)
	 * @author Le Duytam
	 */
	public String getSource() {
		return this.source;
	}
	
	/**
	 * Setter for source with 2 parameters
	 * @param s First String
	 * @param s2 Second String
	 * @author Le Duytam, Liliane 
	 */
	public void setSource(String s, String s2) {
		this.source = s+";"+s2;
	}
	
	/**
	 * Setter for source with 1 parameter
	 * @param s
	 * @author Le Duytam, Liliane
	 */
	public void setSource(String s) {
		this.source = s;
	}
	
	/**
	 * Overriding the toString method 
	 * All values are separated by tabs 
	 * @author Le Duytam
	 */
	@Override
	public String toString() {
		return (this.name+"\t"+this.runtime+"\t"+this.releaseYear+"\t"+this.source);
	}
	
	/**
	 * Overriding the equals method
	 * 2 movies are equal only if the title and the release year are the same and the runtime is within 5 of each other
	 * @author Le Duytam
	 */
	@Override
	public boolean equals(Object o) {
		//Returns false if o is not a Movie
		if(!(o instanceof Movie)) {
			return false;
		}
		
		Movie m = (Movie)o;
		
		//Converting string into a integer 
		int objRuntime = Integer.parseInt(m.runtime);
		int thisRuntime = Integer.parseInt(this.runtime);
		
		//Computing the difference and taking its absolute value
		int differenceRuntime = Math.abs(thisRuntime-objRuntime);
		
		//Boolean value of if the runtime is within 5 of each other
		boolean runtimeWithingRange = differenceRuntime <= 5;
		
		return this.name.equals(m.name) && this.releaseYear.equals(m.releaseYear) && runtimeWithingRange;
	}
	
	/**
	 * Overriding the hashCode method 
	 * Only the title and release year matters for the hash code
	 * @author Le Duytam
	 */
	@Override
	public int hashCode() {
		String mergedString = this.name+this.releaseYear;
		return mergedString.hashCode();
	}
	
	/**
	 * Checks if the movie is valid, runtime can be converted to integer, fields cannot be empty or null
	 * @return boolean value representing if the movie is valid or not 
	 * @author Liliane
	 */
	public boolean isValidMovie() {
		
		if( !(convertStringToInt(this.releaseYear))) {
			return false;
		}
		if(this.releaseYear == "" || this.releaseYear == null) {
			return false;
		}
		if (this.name == "" || this.name == null) {
			return false;
		}
		if ( !(convertStringToInt(this.runtime)) ) {
			return false;
		}
		if(this.runtime == "" || this.runtime == null) {
			return false;
		}
		if(this.source == "" || this.source == null) {
			return false;
		}
		
		return true;

	}
	
	/**
	 * Checks if the a String can be converted into a integer
	 * @param input String to be checked
	 * @return boolean value representing if the string can be converted to a String or not 
	 * @author Liliane
	 */
	public boolean convertStringToInt(String input) {
		try {
			Integer.parseInt(input);
			return true;
		}
		catch (Exception NumberFormatException){
			System.out.println("Cannot convert this String into an integer!");
			return false;
		}
	}
}
