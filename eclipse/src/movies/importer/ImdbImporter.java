/**
 * Class is responsible for importing the movies from the Imdb file
 * @author Liliane
 */

package movies.importer;

import java.util.*;
/**
 * Class extending to the Processor class
 * 
 */
public class ImdbImporter extends Processor{
	
	/**
	 * Variables declaration
	 */
	private final int TOTAL_COLUMNS = 22;
	private final int RELEASE_YEAR_COLUMNS = 3;
	private final int TITLE_COLUMNS = 1;
	private final int RUNTIME_COLUMN = 6;
	private final String SOURCE = "imdb";
	
	/**
	 * Parameterized constructor for the ImdbImporter class
	 * Third value true to ignore first line
	 * @param source (Source directory)
	 * @param destination (Destination directory)
	 */
	public ImdbImporter(String source, String destination) {
		super(source, destination, true);
	}
	
	/**
	 * Process method
	 * @param ArrayList<String> moviesFile (List of movies in the Imdb file)
	 * @return ArrayList<String> of the new processed list
	 */
	public ArrayList<String> process(ArrayList<String> moviesFile){
		ArrayList<String> newMoviesFile = new ArrayList<String>();
		
		
		for(String input : moviesFile) {
			String[] section = input.split("\\t", -1);
			
			if(numColumns(section)) {
			Movie movie_imdb = new Movie(section[RELEASE_YEAR_COLUMNS], section[TITLE_COLUMNS], section[RUNTIME_COLUMN], SOURCE);

			newMoviesFile.add(movie_imdb.toString());
			}
		}
		
		
		return newMoviesFile;
	}
	
	/**
	 * Counting number of columns method to make sure that there is the right length
	 * @param String[] section, taking in the size
	 * @return True or False depending on if it is the right size or not.
	 */
	
	public boolean numColumns(String[] section) {
		
		if (section.length == TOTAL_COLUMNS ) {
			return true;
		}
		
		return false;
		
	}
	
	
}
