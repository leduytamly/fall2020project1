package movies.importer;
/**
 * KaggleImporter is responsible for importing Kaggle files with only a movie's runtime, title, release year, and source
 * @author Le Duytam Ly 
 */
import java.util.ArrayList;

public class KaggleImporter extends Processor{
	//Constant values for the important fields in the kaggle file 
	private final int TOTALCOLS = 21;
	private final int RELEASEYEARCOL = 20;
	private final int TITLECOL = 15;
	private final int RUNTIMECOL = 13;
	private final String SOURCE = "kaggle";
	
	/**
	 * Constructor for KaggleImporter
	 * @param sourceDir Source directory 
	 * @param outputDir Output directory 
	 */
	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}

	/**
	 * Overriding the process method 
	 * @return An ArrayList<String> containing all of the movie's values from its toString
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> processed = new ArrayList<String>();
		//Loop to split every line of the file
		for (String s : input) {
			//Splits every line of the file
			String[] line = s.split("\\t",-1);
			//If there is the right number of columns, it is going create a movie with the correct values and call its to string method 
			if(checkNumberOfCols(line)) {
				Movie m = new Movie(line[RELEASEYEARCOL],line[TITLECOL],line[RUNTIMECOL],SOURCE);
				//adds the output of toString the ArrayList
				processed.add(m.toString());
			}
		}
		return processed;
	}
	
	/**
	 * Checks if the length of an array is equal to the actual number of columns of the file
	 * @param arr String array, result of the split method
	 * @return boolean value representing whether the length is equal to the number of columns
	 */
	public boolean checkNumberOfCols(String[] arr) {
		if(arr.length==TOTALCOLS) {
			return true;
		}
		return false;
	}

}
