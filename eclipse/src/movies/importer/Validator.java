/**
 * Class is responsible for validating the data inside the processed Imdb file.
 * @author Liliane
 */
package movies.importer;

import java.util.*;

/**
 * Class extending to the Processor class 
 */
public class Validator extends Processor{
	
	/**
	 * Variables declaration
	 */
	private final int RELEASE_YEAR_COLUMNS = 2;
	private final int TITLE_COLUMNS = 0;
	private final int RUNTIME_COLUMN = 1;
	private final int SOURCE = 3;
	
	/**
	 * Parameterized constructor for the Validator class
	 * @param source (Source directory)
	 * @param destination (Destination directory)
	 */
	public Validator(String source, String destination) {
		super(source, destination, false);
	}
	
	/**
	 * Process method
	 * @param ArrayList<String> moviesFile (List of movies in the Imdb file)
	 * @return ArrayList<String> of the new processed list
	 */
	public ArrayList<String> process(ArrayList<String> movie){
		// New array list for the validation of movies
		ArrayList<String> newMoviesList = new ArrayList<String>();
		
		//Loop to split the ArrayList (with the condition of tabs and removing empty columns) and putting it in an array
		for(String input : movie) {
			String[] section = input.split("\\t", -1);
		
			// Initiating an object and checking if the data is valid and if it is, it will be added in the new ArrayList created
			Movie mov = new Movie(section[RELEASE_YEAR_COLUMNS], section[TITLE_COLUMNS], section[RUNTIME_COLUMN], section[SOURCE]);
			boolean movResult = mov.isValidMovie();
			
			if (movResult == true) {
				newMoviesList.add(mov.toString());
			}
		
	}
		return newMoviesList;
		
}
}
