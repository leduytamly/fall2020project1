package movies.importer;
/**
 * Normalizes the entries by making the title lowercased and have only the first word in the runtime
 * @author Le Duytam
 */
import java.util.ArrayList;

public class Normalizer extends Processor{
	//Constant values for the fields of the output of toStrong of the Movie class 
	private final int RELEASEYEARCOL = 2;
	private final int TITLECOL = 0;
	private final int RUNTIMECOL = 1;
	private final int SOURCECOL = 3;
	
	/**
	 * Constructor for normalizer
	 * @param sourceDir Source directory 
	 * @param outputDir Output directory 
	 */
	public Normalizer(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	/**
	 * Overriding the process method to normalize the entries
	 * name should have only lower case letters 
	 * runtime should only have the first word
	 * @return ArrayList with normalized entries 
	 */
	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> processed = new ArrayList<String>();
		//Loops to split every line of the file
		for (String s : input) {
			String[] line = s.split("\\t");
			//Creates a movie with lowercased letters and the first word of the runtime
			Movie m = new Movie(line[RELEASEYEARCOL],line[TITLECOL].toLowerCase(),processRuntime(line[RUNTIMECOL]),line[SOURCECOL]);
			//Adds the toString of the normalized movie to the ArrayList
			processed.add(m.toString());
		}
		return processed;
	}
	
	/**
	 * Returns only the first word of runtime
	 * @param runtime The runtime of the movie 
	 * @return The first word of the runtime
	 */
	private String processRuntime(String runtime) {
		String[] runtimeSplit = runtime.split(" ");
		return runtimeSplit[0];
	}
	
}
