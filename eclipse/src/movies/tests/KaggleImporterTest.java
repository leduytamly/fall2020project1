package movies.tests;
/**
 * Test cases for KaggleImporter method
 * @author Le Duytam
 */
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.KaggleImporter;

class KaggleImporterTest {
	/**
	 * Test the process methods
	 */
	@Test
	void testProcess() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Jai Courtney	Bruce Willis	Sebastian Koch	Mary Elizabeth Winstead	Yuliya Snigir	Cole Hauser	\"John McClane (Bruce Willis) heads to Russia in this fifth installment of the Die Hard film series. Skip Woods (The A-Team) provides the script, with Max Payne's John Moore directing. ~ Jeremy Wheeler, Rovi\"	John Moore (VIII)	Director Not Available	Director Not Available	Action	R 	2/14/2013	97 minutes	20th Century Fox	A Good Day To Die Hard	Skip Woods	Writer Not Available	Writer Not Available	Writer Not Available	2013");
		list.add("Tom Arnold	Tim Curry	Dean Stockwell	David Alan Grier	Debra Messing	Ernest Borgnine	\"Based on a 1960s sitcom, this action comedy follows rascally retired naval Lieutenant Quentin McHale as he rejoins the Navy, regathers his former cohorts and sets sail aboard PT 73 to save the world from his old nemesis Vladikov while contending with obstacles presented by his favorite foil Captain Binghampton.\"	Bryan Spicer	Director Not Available	Director Not Available	Action	PG 	4/18/1997	109 minutes	Universal Pictures	McHale's Navy	Peter Crabbe	Writer Not Available	Writer Not Available	Writer Not Available	1997");
		

		KaggleImporter ki = new KaggleImporter(" "," ");
		ArrayList<String> processed = ki.process(list);
		
		String expected1 = "A Good Day To Die Hard"+"\t"+"97 minutes"+"\t"+"2013"+"\t"+"kaggle";
		String expected2 = "McHale's Navy"+"\t"+"109 minutes"+"\t"+"1997"+"\t"+"kaggle";
		
		assertEquals(expected1,processed.get(0));
		assertEquals(expected2,processed.get(1));
	}
	
	/**
	 * Test the process method with the wrong number of columns. The entry that has the wrong number of columns should not be in the returned ArrayList  
	 */
	@Test
	void testProcessWrongCols() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Jai Courtney	Bruce Willis	Sebastian Koch	Mary Elizabeth Winstead	Yuliya Snigir	Cole Hauser	\"John McClane (Bruce Willis) heads to Russia in this fifth installment of the Die Hard film series. Skip Woods (The A-Team) provides the script, with Max Payne's John Moore directing. ~ Jeremy Wheeler, Rovi\"	John Moore (VIII)	Director Not Available	Director Not Available	Action	R 	2/14/2013	97 minutes	20th Century Fox	A Good Day To Die Hard	Skip Woods	Writer Not Available	Writer Not Available	Writer Not Available	2013");
		list.add("Tim Curry	Dean Stockwell	David Alan Grier	Debra Messing	Ernest Borgnine	\"Based on a 1960s sitcom, this action comedy follows rascally retired naval Lieutenant Quentin McHale as he rejoins the Navy, regathers his former cohorts and sets sail aboard PT 73 to save the world from his old nemesis Vladikov while contending with obstacles presented by his favorite foil Captain Binghampton.\"	Bryan Spicer	Director Not Available	Director Not Available	Action	PG 	4/18/1997	109 minutes	Universal Pictures	McHale's Navy	Peter Crabbe	Writer Not Available	Writer Not Available	Writer Not Available	1997");
		list.add("Robert Mitchum	Deborah Kerr	Cast Not Available	Cast Not Available	Cast Not Available	Cast Not Available	\"A two-person character study directed by John Huston, Heaven Knows Mr. Allison stars Robert Mitchum as a World War II Marine sergeant and Deborah Kerr as a Roman Catholic nun. Both nun and sergeant are marooned on a South Pacific island, hemmed in by surrounding Japanese troops. Mitchum does his best to make the nun's ordeal less painful, but is torn by his growing love for her. Kerr is equally fond of Mitchum, but refuses to renounce her vows. Their unrealized ardor mellows into mutual respect as they struggle to survive before help arrives. Based on a novel by Charles K. Shaw, Heaven Knows, Mr. Allison was coproduced by Eugene Frenke, who later filmed a low-budget variation on the story, The Nun and the Sergeant (62), which starred Frenke's wife Anna Sten. ~ Hal Erickson, Rovi\"	John Huston	Director Not Available	Director Not Available	Action	NR	1/1/1957	108 minutes	Fox	Heaven Knows Mr. Allison	John Lee Mahin	John Huston	Writer Not Available	Writer Not Available	1957");
		

		KaggleImporter ki = new KaggleImporter(" "," ");
		ArrayList<String> processed = ki.process(list);
		
		String expected1 = "A Good Day To Die Hard"+"\t"+"97 minutes"+"\t"+"2013"+"\t"+"kaggle";
		String expected2 = "Heaven Knows Mr. Allison"+"\t"+"108 minutes"+"\t"+"1957"+"\t"+"kaggle";
		
		assertEquals(expected1,processed.get(0));
		assertEquals(expected2,processed.get(1));
	}
	
	/**
	 * Test if the array length has the same value of the total number of columns 
	 */
	@Test 
	void testCheckNumberOfCols(){
		String s = "Jai Courtney	Bruce Willis	Sebastian Koch	Mary Elizabeth Winstead	Yuliya Snigir	Cole Hauser	\"John McClane (Bruce Willis) heads to Russia in this fifth installment of the Die Hard film series. Skip Woods (The A-Team) provides the script, with Max Payne's John Moore directing. ~ Jeremy Wheeler, Rovi\"	John Moore (VIII)	Director Not Available	Director Not Available	Action	R 	2/14/2013	97 minutes	20th Century Fox	A Good Day To Die Hard	Skip Woods	Writer Not Available	Writer Not Available	Writer Not Available	2013";
		
		String[] sSplit = s.split("\\t");
		
		KaggleImporter ki = new KaggleImporter(" "," ");
		boolean result = ki.checkNumberOfCols(sSplit);
		boolean expected = true;
		assertEquals(expected,result);
	}
	
	/**
	 * Test if the checkNumberOfCols returns false if the length of the array is the wrong number of columns 
	 */
	@Test 
	void testCheckNumberOfColsWrongCols(){
		String s = "Bruce Willis	Sebastian Koch	Mary Elizabeth Winstead	Yuliya Snigir	Cole Hauser	\"John McClane (Bruce Willis) heads to Russia in this fifth installment of the Die Hard film series. Skip Woods (The A-Team) provides the script, with Max Payne's John Moore directing. ~ Jeremy Wheeler, Rovi\"	John Moore (VIII)	Director Not Available	Director Not Available	Action	R 	2/14/2013	97 minutes	20th Century Fox	A Good Day To Die Hard	Skip Woods	Writer Not Available	Writer Not Available	Writer Not Available	2013";
		
		String[] sSplit = s.split("\\t");
		
		KaggleImporter ki = new KaggleImporter(" "," ");
		boolean result = ki.checkNumberOfCols(sSplit);
		boolean expected = false;
		assertEquals(expected,result);
	}

}
