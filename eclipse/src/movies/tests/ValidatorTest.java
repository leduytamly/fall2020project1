/**
 * JUnit Tests responsible for testing Validator class
 * @author Liliane
 */

package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import org.junit.jupiter.api.Test;

import movies.importer.Validator;


class ValidatorTest {
	
	/**
	 * JUnit Tests testing if the process is working properly.
	 * Tests for two movies only
	 */
	@Test
	void processTest() {
		
		ArrayList<String> movieTest = new ArrayList <String>();
		movieTest.add("Miss Jerry	45	1894	imdb");
		movieTest.add("The Story of the Kelly Gang	70	1906	imdb");
		
		Validator val = new Validator(" ", " ");
		ArrayList<String> newMovieTest = val.process(movieTest);
		
		String first = "Miss Jerry	45	1894	imdb";
		String second = "The Story of the Kelly Gang	70	1906	imdb";
		
		
		assertEquals(first, newMovieTest.get(0));
		assertEquals(second, newMovieTest.get(1));
	}
	
	/**
	 * JUnit Tests testing if the process is working properly.
	 * Tests for 3 movies (2 being almost the same)
	 * Also testing if it will be able to remove the "m"
	 */
	@Test
	void processTes2() {
		
		ArrayList<String> movieTest = new ArrayList <String>();
		movieTest.add("Miss Jerry	45	1894	imdb");
		movieTest.add("The Story of the Kelly Gang	70m	1906	imdb");
		movieTest.add("The Story of the Kelly Gang	70	1906	imdb");
		
		
		Validator val = new Validator(" ", " ");
		ArrayList<String> newMovieTest = val.process(movieTest);
		
		String first = "Miss Jerry	45	1894	imdb";
		String second = "The Story of the Kelly Gang	70	1906	imdb";
		
		
		assertEquals(first, newMovieTest.get(0));
		assertEquals(second, newMovieTest.get(1));
	}
}
