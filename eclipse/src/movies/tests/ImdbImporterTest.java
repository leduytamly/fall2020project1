/**
 * JUnit Tests responsible for testing each methods
 * @author Liliane
 */

package movies.tests;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import movies.importer.ImdbImporter;


public class ImdbImporterTest {
	
	/**
	 * JUnit Tests testing if the process is working properly.
	 */
	@Test
	void processTest() {
		ArrayList<String> movieTest = new ArrayList <String>();
		movieTest.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2");
		movieTest.add("tt0000574	The Story of the Kelly Gang	The Story of the Kelly Gang	1906	12/26/1906	\"Biography, Crime, Drama\"	70	Australia	None	Charles Tait	Charles Tait	J. and N. Tait	\"Elizabeth Tait, John Tait, Norman Campbell, Bella Cola, Will Coyne, Sam Crewes, Jack Ennis, John Forde, Vera Linden, Mr. Marshall, Mr. McKenzie, Frank Mills, Ollie Wilson\"	True story of notorious Australian outlaw Ned Kelly (1855-80).	6.1	589	\"$2,250 \"				7	7");
		
		ImdbImporter imdb = new ImdbImporter(" ", " ");
		ArrayList<String> newMovieTest = imdb.process(movieTest);
		
		String first = "Miss Jerry"+"\t"+"45"+"\t"+"1894"+"\t"+"imdb";
		String second = "The Story of the Kelly Gang"+"\t"+"70"+"\t"+"1906"+"\t"+"imdb";
		
		
		assertEquals(first, newMovieTest.get(0));
		assertEquals(second, newMovieTest.get(1));
		
		
	}
	
	/**
	 * JUnit Tests testing if the process is working properly.
	 */
	@Test
	void processTest2() {
		ArrayList<String> movieTest = new ArrayList <String>();
		movieTest.add("tt0002452	Independenta Romaniei	Independenta Romaniei	1912	9/1/1912	\"History, War\"	120	Romania		\"Aristide Demetriade, Grigore Brezeanu\"	\"Aristide Demetriade, Petre Liciu\"	Societatea Filmului de Arta Leon Popescu	\"Aristide Demetriade, Constanta Demetriade, Constantin Nottara, Pepi Machauer, Aurel Athanasescu, Jeny Metaxa-Doro, Nicolae Soreanu, Vasile Toneanu, Aristita Romanescu, Elvire Popesco, M. Vîrgolici, C. Nedelcovici, Mihail Tancovici-Cosmin, Ion Dumitrescu, Gheorghe Meliseanu\"	The movie depicts the Romanian War of Independence (1877-1878).	6.7	198	ROL 400000				4	1\r\n" + "");
		movieTest.add("tt0002445	Quo Vadis?	Quo Vadis?	1913	3/1/1913	\"Drama, History\"	120	Italy	Italian	Enrico Guazzoni	\"Henryk Sienkiewicz, Enrico Guazzoni\"	Società Italiana Cines	\"Amleto Novelli, Gustavo Serena, Carlo Cattaneo, Amelia Cattaneo, Lea Giunchi, Bruto Castellani, Augusto Mastripietri, Cesare Moltini, Olga Brandini, Ignazio Lupi, Giovanni Gizzi, Lia Orlandini, Matilde Guillaume, Ida Carloni Talli, Giuseppe Gambardella\"	\"An epic Italian film \"\"Quo Vadis\"\" influenced many of the later movies.\"	6.2	273	ITL 45000				7	5\r\n" + "");
		
				ImdbImporter imdb = new ImdbImporter(" ", " ");
		ArrayList<String> newMovieTest = imdb.process(movieTest);
		
		String first = "Independenta Romaniei"+"\t"+"120"+"\t"+"1912"+"\t"+"imdb";
		String second = "Quo Vadis?"+"\t"+"120"+"\t"+"1913"+"\t"+"imdb";
		
		
		assertEquals(first, newMovieTest.get(0));
		assertEquals(second, newMovieTest.get(1));
		
		
	}
	
	/**
	 * JUnit Tests testing if the code will know if it is the wrong number of columns in the process method
	 */
	@Test
	void processTestWrongCols() {
		ArrayList<String> movieTest = new ArrayList <String>();
		movieTest.add("tt0001892	Den sorte drøm	Den sorte drøm	1911	8/19/1911	Drama	53	\"Germany, Denmark\"		Urban Gad	\"Urban Gad, Gebhard Schätzler-Perasini\"	Fotorama	\"Asta Nielsen, Valdemar Psilander, Gunnar Helsengreen, Emil Albes, Hugo Flink, Mary Hagen\"	\"Two men of high rank are both wooing the beautiful and famous equestrian acrobat Stella. While Stella ignores the jeweler Hirsch, she accepts Count von Waldberg's offer to follow her home, ...\"	5.8	188					5	2");
		movieTest.add("tt0002101	Cleopatra	Cleopatra	1912	11/13/1912	\"Drama, History\"	100	USA	English	Charles L. Gaskill	Victorien Sardou	Helen Gardner Picture Players	\"Helen Gardner, Pearl Sindelar, Miss Fielding, Miss Robson, Helene Costello, Charles Sindelar, Mr. Howard, James R. Waite, Mr. Osborne, Harry Knowles, Mr. Paul, Mr. Brady, Mr. Corker\"	The fabled queen of Egypt's affair with Roman general Marc Antony is ultimately disastrous for both of them.	5.2	446	\"$45,000 \"				25	3");
		
		ImdbImporter imdb = new ImdbImporter(" ", " ");
		ArrayList<String> newMovieTest = imdb.process(movieTest);
		
		String first = "Den sorte drøm"+"\t"+"53"+"\t"+"1911"+"\t"+"imdb";
		String second = "Cleopatra"+"\t"+"100"+"\t"+"1912"+"\t"+"imdb";
		
		
		assertEquals(first, newMovieTest.get(0));
		assertEquals(second, newMovieTest.get(1));
	}
		
	/**
	 * JUnit Tests testing if the code will know if it is the right number of columns
	 */
	@Test
	void checkColumnsTest() {
		String movie = "tt0002101	Cleopatra	Cleopatra	1912	11/13/1912	\"Drama, History\"	100	USA	English	Charles L. Gaskill	Victorien Sardou	Helen Gardner Picture Players	\"Helen Gardner, Pearl Sindelar, Miss Fielding, Miss Robson, Helene Costello, Charles Sindelar, Mr. Howard, James R. Waite, Mr. Osborne, Harry Knowles, Mr. Paul, Mr. Brady, Mr. Corker\"	The fabled queen of Egypt's affair with Roman general Marc Antony is ultimately disastrous for both of them.	5.2	446	\"$45,000 \"				25	3";
		
		String[] movieSplit = movie.split("\\t");
		
		ImdbImporter imdb = new ImdbImporter(" ", " ");
		boolean result = imdb.numColumns(movieSplit);
		boolean expected = true;
		
		assertEquals(expected, result);

	}
	
	/**
	 * JUnit Tests testing if the code will know if it is the wrong number of columns
	 */
	@Test
	void checkWrongColTest() {
		String movie = "Cleopatra	Cleopatra	1912	11/13/1912	\"Drama, History\"	100	USA	English	Charles L. Gaskill	Victorien Sardou	Helen Gardner Picture Players	\"Helen Gardner, Pearl Sindelar, Miss Fielding, Miss Robson, Helene Costello, Charles Sindelar, Mr. Howard, James R. Waite, Mr. Osborne, Harry Knowles, Mr. Paul, Mr. Brady, Mr. Corker\"	The fabled queen of Egypt's affair with Roman general Marc Antony is ultimately disastrous for both of them.	5.2	446	\"$45,000 \"				25	3";
		
		String[] movieSplit = movie.split("\\t");
		
		ImdbImporter imdb = new ImdbImporter(" ", " ");
		boolean result = imdb.numColumns(movieSplit);
		boolean expected = false;
		
		assertEquals(expected, result);
	}
	
}
