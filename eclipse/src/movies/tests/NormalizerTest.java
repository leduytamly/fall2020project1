package movies.tests;
/**
 * Test cases for Normalizer
 * @author Le Duytam Ly
 */
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Normalizer;

class NormalizerTest {
	/**
	 * Testing the normalizer for Kaggle entries 
	 */
	@Test
	void testProcessKaggle() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("The Masked Saint	111 minutes	2016	kaggle");
		list.add("Mortdecai	106 minutes	2015	kaggle");
		
		Normalizer n = new Normalizer(" "," ");
		ArrayList<String> processed = n.process(list);
		
		String expected1 = "the masked saint"+"\t"+"111"+"\t"+"2016"+"\t"+"kaggle";
		String expected2 = "mortdecai"+"\t"+"106"+"\t"+"2015"+"\t"+"kaggle";
		
		assertEquals(expected1,processed.get(0));
		assertEquals(expected2,processed.get(1));
	}
	
	/**
	 * Testing the normalizer for IMDB entries 
	 */
	@Test
	void testProcessImdb() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("Miss Jerry	45	1894	imdb");
		list.add("The Patchwork Girl of Oz	81	1914	imdb");
		
		Normalizer n = new Normalizer(" "," ");
		ArrayList<String> processed = n.process(list);
		
		String expected1 = "miss jerry"+"\t"+"45"+"\t"+"1894"+"\t"+"imdb";
		String expected2 = "the patchwork girl of oz"+"\t"+"81"+"\t"+"1914"+"\t"+"imdb";
		
		assertEquals(expected1,processed.get(0));
		assertEquals(expected2,processed.get(1));
	}
	
	/**
	 * Testing the processRuntime method
	 */
	@Test
	void testProcessRuntime() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("The Masked Saint	111 minutes	2016	kaggle");
		Normalizer n = new Normalizer(" "," ");
		ArrayList<String> processed = n.process(list);
		String expected = "111";
		String actual = processed.get(0).split("\\t")[1];
		assertEquals(expected,actual);
	}
	
	/**
	 * Testing the processRuntime method
	 */
	@Test
	void testProcessRuntimeWordAsFirstWord() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("The Masked Saint	minutes 111	2016	kaggle");
		Normalizer n = new Normalizer(" "," ");
		ArrayList<String> processed = n.process(list);
		String expected = "minutes";
		String actual = processed.get(0).split("\\t")[1];
		assertEquals(expected,actual);
	}

}
