/**
 * JUnit Tests responsible for testing the duplicate in the deduper method
 * @author Le Duytam
 */

package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import movies.importer.Deduper;

import org.junit.jupiter.api.Test;

class DeduperTest {

	/**
	 * Test the removeDuplicates with 2 kaggle and 2 imdb source
	 */
	@Test
	void testRemoveDuplicates() {
		String test = "kaggle;imdb;kaggle;imdb";
		Deduper d = new Deduper("","");
		String actual = d.removeDuplicate(test);
		String expected = "kaggle;imdb";
		assertEquals(expected,actual);
	}
	
	/**
	 * Test the removeDuplicates with 2 kaggle source
	 */
	@Test
	void testRemoveDuplicates2() {
		String test = "kaggle;kaggle;imdb";
		Deduper d = new Deduper("","");
		String actual = d.removeDuplicate(test);
		String expected = "kaggle;imdb";
		assertEquals(expected,actual);
	}
	
	/**
	 * Test the remove duplicates with all the same source
	 */
	@Test
	void testRemoeDuplicates3() {
		String test = "imdb;imdb;imdb";
		Deduper d = new Deduper("","");
		String actual = d.removeDuplicate(test);
		String expected = "imdb";
		assertEquals(expected,actual);
	}
	
	/**
	 * Test the process method for Deduper. It should merge all three movies since they are the same (with runtime within 5) 
	 */
	@Test
	void testProcess() {
		ArrayList<String> movieTest = new ArrayList <String>();
		movieTest.add("Miss Jerry	45	1894	imdb");
		movieTest.add("Miss Jerry	47	1894	kaggle");
		movieTest.add("Miss Jerry	43	1894	kaggle");
		
		Deduper d = new Deduper(" ", " ");
		ArrayList<String> merged = d.process(movieTest);
		String expected = "Miss Jerry	45	1894	kaggle;imdb";
		assertEquals(expected,merged.get(0));
	}

}
