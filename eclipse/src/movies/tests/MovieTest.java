package movies.tests;
import movies.importer.Movie;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 * Test cases for Movie
 */
class MovieTest {
	/**
	 * Testing the getSource method
	 * @author Le Duytam
	 */
	@Test
	void testGetSource() {
		Movie m = new Movie("2000","Movie Name","2 hours","tomato");
		assertEquals("tomato",m.getSource());
	}
	
	/**
	 * Testing setSource method with 2 parameters 
	 * @author Le Duytam
	 */
	@Test
	void testSetSource2Param() {
		Movie m = new Movie("2000","Movie Name","2 hours","kaggle");
		m.setSource("imdb", "kaggle");
		String expected = "imdb;kaggle";
		assertEquals(expected,m.getSource());
	}
	
	/**
	 * Testing setSource method with 1 parameter 
	 * @author Le Duytam
	 */
	@Test
	void testSetSource1Param() {
		Movie m = new Movie("2000","Movie Name","2 hours","kaggle");
		m.setSource("imdb;kaggle");
		String expected = "imdb;kaggle";
		assertEquals(expected,m.getSource());
	}
	
	/**
	 * Testing the toString method 
	 * @author Le Duytam
	 */
	@Test
	void testToString() {
		Movie m = new Movie("2000","Movie Name","2 hours","tomato");
		String expected = "Movie Name"+"\t"+"2 hours"+"\t"+"2000"+"\t"+"tomato";
		String result = m.toString();
		assertEquals(expected,result);
	}
	
	/**
	 * Testing the equals method
	 * It should return true since its the same exact movie
	 * @author Le Duytam
	 */
	@Test 
	void testEquals() {
		Movie m1 = new Movie("2000","Movie Name","2","tomato");
		Movie m2 = new Movie("2000","Movie Name","2","tomato");
		assertEquals(m1,m2);
	}
	
	/**
	 * Testing equals method when the runtime and source is different
	 * It should should still be equal to each other since the release year and title are the same
	 * The runtime is within 5 minutes apart (+5)
	 * @author Le Duytam
	 */
	@Test 
	void testEqualsRuntimeUpperBound() {
		Movie m1 = new Movie("2000","Movie Name","6","tomato");
		Movie m2 = new Movie("2000","Movie Name","11","imdb");
		assertEquals(m1,m2);
	}
	
	/**
	 * Testing equals method when the runtime and source is different
	 * It should should still be equal to each other since the release year and title are the same
	 * The runtime is within 5 minutes apart (-5) 
	 * @author Le Duytam
	 */
	@Test 
	void testEqualsRuntimeLowerBound() {
		Movie m1 = new Movie("2000","Movie Name","6","tomato");
		Movie m2 = new Movie("2000","Movie Name","1","imdb");
		assertEquals(m1,m2);
	}
	
	/**
	 * Testing the overwritten hashCode method
	 * Hash code should be the same since its the same exact same movie
	 * @author Le Duytam
	 */
	@Test 
	void testHashCode() {
		Movie m1 = new Movie("2000","Movie Name","2 hours","tomato");
		Movie m2 = new Movie("2000","Movie Name","2 hours","tomato");
		int hashCode1 = m1.hashCode();
		int hashCode2 = m2.hashCode();
		assertEquals(hashCode1,hashCode2);
	}
	
	/**
	 * Testing the overwritten hashCode method
	 * Hash code should be the same since the release year and title are the same 
	 * @author Le Duytam
	 */
	@Test 
	void testHashCodeWrongValues() {
		Movie m1 = new Movie("2000","Movie Name","3 hours","tomato");
		Movie m2 = new Movie("2000","Movie Name","2 hours","imdb");
		int hashCode1 = m1.hashCode();
		int hashCode2 = m2.hashCode();
		assertEquals(hashCode1,hashCode2);
	}
	
	/**
	 * Testing if the runtime and released year can be converted from String to Int
	 * @author Liliane
	 */
	@Test
	void testConvertStringToInt() {
		Movie mov = new Movie("Miss Jerry", "45", "1894", "imdb");
		boolean result = mov.convertStringToInt("45");
		
		
		assertEquals(true, result);
	}
	
	/**
	 * Testing if the movie is valid in the Validator
	 * @author Liliane
	 */
	@Test
	void testIsValidMovie() {
		Movie mov = new Movie("1906", "The Story of the Kelly Gang", "70", "imdb");
		boolean result = mov.isValidMovie();
		
		
		assertEquals(true, result);
	}
	
	/**
	 * Testing if the movie is NOT valid in the Validator
	 * @author Liliane
	 */
	@Test
	void testIsValidMovie2() {
		Movie mov = new Movie("1894", "", "45", "imdb");
		boolean result = mov.isValidMovie();
		
		
		assertEquals(false, result);
	}
}
